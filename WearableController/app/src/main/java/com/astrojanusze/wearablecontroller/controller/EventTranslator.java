package com.astrojanusze.wearablecontroller.controller;

import android.util.Log;

import com.astrojanusze.wearablecontroller.R;

/**
 * Created by Deimos on 2016-04-23.
 */
public class EventTranslator {

    String logtag = "EVENT";
    BluetoothWorker mBTWorker;

    public static int BUTTON_ROLLER = 0;
    public static int BUTTON_PANIC = 1;

    public static int EVENT_CLICK = 0;
    public static int EVENT_LONGPRESS = 1;
    public static int EVENT_LEFT = 2;
    public static int EVENT_RIGHT = 3;

    public EventTranslator() {
        mBTWorker = BluetoothWorker.getInstance();
    }

    public void buttonEvent(int id, int action) {

        int buttonId = formatButton(id);
        int eventId = -1;

        if (eventId != -1 && buttonId != -1)
            forwardEvent(buttonId, eventId);
    }

    public void onSimpleClick(int id) {
        int buttonId = formatButton(id);
        int eventId = EVENT_CLICK;

        switch (id) {
            case R.id.bLeft:
                eventId = EVENT_LEFT;
                break;
            case R.id.bRight:
                eventId = EVENT_RIGHT;
                break;
        }

        if (buttonId != -1)
           forwardEvent(buttonId, eventId);
    }

    public void onLongClick(int id) {
        int buttonId = formatButton(id);
        int eventId = EVENT_LONGPRESS;

        if ( buttonId != -1)
            forwardEvent(buttonId, eventId);
    }

    private int formatButton(int viewId) {
        int buttonId = -1;

        switch (viewId) {
            case R.id.bLeft:
            case R.id.bRight:
            case R.id.bSelect:
                buttonId = BUTTON_ROLLER;
                break;
            case R.id.bPanic:
                buttonId = BUTTON_PANIC;
                break;
        }

        return buttonId;
    }

    private void forwardEvent(final int buttonId, final int eventId) {
        Log.i(logtag, "Button: " + buttonId + ", Event: " + eventId);

        if (mBTWorker.isConnected()) {
            StringBuilder sb = new StringBuilder(buttonId);
            sb.append(eventId);
            mBTWorker.sendMessage(sb.toString());
        }
    }

}
