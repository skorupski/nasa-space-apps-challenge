package com.astrojanusze.wearablecontroller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.astrojanusze.wearablecontroller.controller.BluetoothWorker;
import com.astrojanusze.wearablecontroller.controller.EventTranslator;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {

    Button bPanic, bLeft, bRight, bSelect, bBT;
    EventTranslator mController;
    BluetoothWorker mBTWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBTWorker = BluetoothWorker.init(this);
        mController = new EventTranslator();
        loadControls();
    }

    private void loadControls() {

        bPanic = (Button) findViewById(R.id.bPanic);
        bLeft = (Button) findViewById(R.id.bLeft);
        bRight = (Button) findViewById(R.id.bRight);
        bSelect = (Button) findViewById(R.id.bSelect);
        bBT = (Button) findViewById(R.id.bConnect);

        bPanic.setOnClickListener(this);
        bLeft.setOnClickListener(this);
        bRight.setOnClickListener(this);
        bSelect.setOnClickListener(this);
        bSelect.setOnLongClickListener(this);

        bBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBTWorker != null)
                    mBTWorker.connect();
            }
        });
    }


    @Override
    public void onClick(View v) {

        if (mController != null)
            mController.onSimpleClick(v.getId());
    }

    @Override
    public boolean onLongClick(View v) {
        if (mController != null) {
            mController.onLongClick(v.getId());
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        if (mBTWorker != null)
            mBTWorker.disconnect();
        super.onDestroy();
    }
}
