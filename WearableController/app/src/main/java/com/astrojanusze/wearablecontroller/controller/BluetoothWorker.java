package com.astrojanusze.wearablecontroller.controller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Deimos on 2016-04-23.
 */
public class BluetoothWorker {

    String UID_STRING = "00001101-0000-1000-8000-00805F9B34FB";
    UUID UID = UUID.fromString(UID_STRING);

    String mainDevice = "XT1068";
    BluetoothAdapter mAdapter;
    BluetoothDevice mDevice;
    ConnectThread mConnectionThread;
    ConnectedThread mMessagingThread;
    Context mContext;

    private static BluetoothWorker instance;

    public static BluetoothWorker init(Context ctx) {
        if (instance == null)
            instance = new BluetoothWorker(ctx);

        return instance;
    }

    public static BluetoothWorker getInstance() {
        return instance;
    }

    private BluetoothWorker(Context ctx) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mContext = ctx;
    }

    public void connect() {
        getMainDevice();
        if (mDevice != null) {
            mConnectionThread = new ConnectThread(mDevice);
            mConnectionThread.run();
        }
    }

    public void disconnect() {
        mConnectionThread.cancel();
        mMessagingThread.cancel();
    }

    public boolean isConnected() {
        return (mConnectionThread == null ? false : mConnectionThread.isConnected());
    }

    public void sendMessage(String msg) {
        mMessagingThread.write(msg.getBytes());
    }

    private void getMainDevice() {
        Set<BluetoothDevice> pairedDevices = mAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (mainDevice.equals(device.getName())) {
                    mDevice = device;
                    return;
                }
            }
        }
    }

    private void manageConnection(BluetoothSocket socket) {
        mMessagingThread = new ConnectedThread(socket);
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;
            try {
                tmp = device.createRfcommSocketToServiceRecord(UID);
            } catch (IOException e) { }
            mmSocket = tmp;
        }

        public void run() {

            try {
                mmSocket.connect();
            } catch (IOException connectException) {
                try {
                    mmSocket.close();
                } catch (IOException closeException) { }
                return;
            }
            manageConnection(mmSocket);
        }

        public void cancel() {
            try {
                mmSocket.close();

            } catch (IOException e) { }
        }

        public boolean isConnected() {
            return (mmSocket == null ? false : mmSocket.isConnected());
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            OutputStream tmpOut = null;

            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmOutStream = tmpOut;
        }

        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

}
