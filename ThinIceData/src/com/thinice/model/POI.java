package com.thinice.model;

public class POI {

    public enum Type {
        ThinIce,
        IceHole,
        Crevass,
        HuntingGame,
        DangerousAnimals,
        Danger,
        Unknown
    }
    
    public int id;
    public int userId;
    public long timestamp;
    public Type type;
    public int dangerLevel;
    public double lat;
    public double lon;
    
    public POI() {
        this.type = Type.Unknown;
    }
    
    public POI(int id, int userId, long timestamp, int typeId, int dangerLevel, long lat, long lon) {
        this.id = id;
        this.timestamp = timestamp;
        this.userId = userId;
        this.type = aquirePOITypeById(typeId);
        this.dangerLevel = dangerLevel;
        this.lat = lat;
        this.lon = lon;
    }
    
    public static Type aquirePOITypeById(int typeId) {
        if (typeId >= 0 && typeId < Type.values().length) {
            return Type.values()[typeId];
        } else {
            return Type.Unknown;
        }
    }
}
