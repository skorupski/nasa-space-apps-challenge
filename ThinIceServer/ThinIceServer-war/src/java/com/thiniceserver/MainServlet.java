package com.thiniceserver;

import com.thiniceserver.db.DBAccess;
import com.thiniceserver.model.POI;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
//        ArrayList<POI> poiList = new ArrayList<>();
//        for (int i = 0; i < 3; ++i) {
//            POI p = new POI();
//            p.id = i;
//            poiList.add(p);
//        }
  
//        POI p = new POI();
//        p.id = 20;
//        
//        String str = "[{\"id\":0,\"userId\":0,\"timestamp\":0,\"dangerLevel\":0,\"type\":6,\"lat\":0.0,\"lon\":0.0},{\"id\":1,\"userId\":0,\"timestamp\":0,\"dangerLevel\":0,\"type\":6,\"lat\":0.0,\"lon\":0.0},{\"id\":2,\"userId\":0,\"timestamp\":0,\"dangerLevel\":0,\"type\":6,\"lat\":0.0,\"lon\":0.0}]";
//        
//        JsonArray arr = Json.createReader(new StringReader(str)).readArray();
////        JsonObject obj = arr.getJsonObject(1);
////        POI t = POI.fromJson(obj);
//        
//        ArrayList<POI> lst = POI.fromJsonArray(arr);
//
//        resp.setContentType("application/json");
//        resp.getWriter().print(POI.arrayListToJson(lst));

        DBAccess access;
        try {
            resp.getWriter().println("Start!");
            access = new DBAccess();
            POI poi = new POI();
            poi.userId = 8;
            poi.lat = 16;
            poi.lon = 52;
            poi.timestamp = 1234;
            poi.type = POI.Type.IceHole;
            poi.dangerLevel = 1;

//            resp.getWriter().println("Adding!");
//            poi = access.addPOI(poi);
//            resp.getWriter().println("POI id: " + poi.id);
            ArrayList<POI> poIs = access.getPOIs(1000);
            resp.getWriter().println("POIs: "  + poIs.size());
            for( POI mypoi : poIs ) {
                resp.getWriter().println("POI: "  + mypoi.id);
            }
            resp.getWriter().println("DONE!");
        } catch (InstantiationException ex) {
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
