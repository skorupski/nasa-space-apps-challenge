/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thiniceserver;

import com.thiniceserver.db.DBAccess;
import com.thiniceserver.model.POI;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class POIAddServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String data = req.getParameter("data");
            if (data != null) {
                DBAccess db = DBAccess.create();
                JsonObject jsonObject = Json.createReader(new StringReader(data)).readObject();
                POI poi = db.addPOI(POI.fromJson(jsonObject));
                resp.setContentType("application/json");
                resp.getWriter().print(POI.toJson(poi));
            }
        } catch (SQLException ex) {
            Logger.getLogger(POIServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
