package com.thiniceserver.db;

public class DBConfig {
    
    final static String DB_URL = "jdbc:mysql://10.200.35.95:3306/thinicedata";
    final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    
    final static String DB_USER = "thinice";
    final static String DB_PASS = "spacedata";

    public static abstract class POIEntry {
        public static final String TABLE_NAME = "POI";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_USER = "user";
        public static final String COLUMN_LAT = "lat";
        public static final String COLUMN_LON = "lon";
        public static final String COLUMN_TIME = "time";
        public static final String COLUMN_TYPE = "type";
        public static final String COLUMN_DANGER = "danger";
    }
}
