package com.thiniceserver.db;

import com.thiniceserver.model.POI;
import java.util.ArrayList;
import java.util.List;

import com.thiniceserver.MainServlet;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBAccess {
    
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    
    public DBAccess() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        Class.forName(DBConfig.JDBC_DRIVER).newInstance();

        con = DriverManager.getConnection(DBConfig.DB_URL, DBConfig.DB_USER, DBConfig.DB_PASS);
        String statement = "SELECT * FROM thinicedata.test";
        st = con.prepareStatement(statement);
        rs = st.executeQuery();
        while (rs.next()) {
            int i = rs.getInt("myid");
            String s = rs.getString("textview");
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, "Entry: " + i + " " + s);
        }
        closeQuery();
    }
    
    public static DBAccess create()
    {
        try {
            DBAccess db = new DBAccess();
            return db;
        } catch (InstantiationException ex) {
            Logger.getLogger(DBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void closeQuery() throws SQLException {
        closeQuery(true);
    }

    private void closeQuery(boolean hasResults) throws SQLException {
        if (hasResults) {
            if (rs != null) {
                rs.close();
                rs = null;
            }
        }

        if (st != null) {
            st.close();
            st = null;
        }
    }

    public void dispose() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (st != null) {
            st.close();
        }
        if (con != null) {
            con.close();
        }
    }
    
    public void createTablePOIs() throws SQLException{
        String statement = "CREATE TABLE " + 
            DBConfig.POIEntry.TABLE_NAME + "(" + 
            DBConfig.POIEntry.COLUMN_ID + " SERIAL PRIMARY KEY, " + 
            DBConfig.POIEntry.COLUMN_USER + " integer, " +
            DBConfig.POIEntry.COLUMN_LAT + " DOUBLE, " +
            DBConfig.POIEntry.COLUMN_LON + " DOUBLE, " +
            DBConfig.POIEntry.COLUMN_TIME + " BIGINT, " +
            DBConfig.POIEntry.COLUMN_TYPE + " integer, " +
            DBConfig.POIEntry.COLUMN_DANGER + " integer)";
        st = con.prepareStatement(statement);
        st.execute();
        closeQuery(false);
    }
	
    public List<POI> getAlerts(long fromTimestamp) {
        ArrayList<POI> alerts = new ArrayList<POI>();
        return alerts;
    }

    public POI addPOI(POI poi) throws SQLException {
        String statement = "INSERT INTO " + 
            DBConfig.POIEntry.TABLE_NAME + "(" + 
            DBConfig.POIEntry.COLUMN_USER + ", " +
            DBConfig.POIEntry.COLUMN_LAT + ", " +
            DBConfig.POIEntry.COLUMN_LON + ", " +
            DBConfig.POIEntry.COLUMN_TIME + ", " +
            DBConfig.POIEntry.COLUMN_TYPE + ", " +
            DBConfig.POIEntry.COLUMN_DANGER + ")" +
            " VALUES ( ?, ?, ?, ?, ?, ? )";
        st = con.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS);
        st.setInt(1, poi.userId); //TODO: ID SHOULD BE LONG!
        st.setDouble(2, poi.lat);
        st.setDouble(3, poi.lon);
        st.setLong(4, poi.timestamp);
        st.setInt(5, poi.type.ordinal());
        st.setInt(6, poi.dangerLevel);
        st.executeUpdate();
        rs = st.getGeneratedKeys();

        if (rs.next()) {
            poi.id = (int)rs.getLong(1); //TODO: ID SHOULD BE LONG!
        } else {
            poi = null;
        }
        closeQuery();

        return poi;
    }
    
    public ArrayList<POI> getPOIs(long from) throws SQLException {
        ArrayList<POI> result = new ArrayList<>();
        String statement = "SELECT " + 
            DBConfig.POIEntry.COLUMN_ID + ", " +
            DBConfig.POIEntry.COLUMN_USER + ", " +
            DBConfig.POIEntry.COLUMN_LAT + ", " +
            DBConfig.POIEntry.COLUMN_LON + ", " +
            DBConfig.POIEntry.COLUMN_TIME + ", " +
            DBConfig.POIEntry.COLUMN_TYPE + ", " +
            DBConfig.POIEntry.COLUMN_DANGER +
            " FROM " + 
            DBConfig.POIEntry.TABLE_NAME +
            " WHERE " + 
            DBConfig.POIEntry.COLUMN_TIME + "> ?";

        st = con.prepareStatement(statement);
        st.setLong(1, from);
        rs = st.executeQuery();
        while (rs.next()) {
            POI poi = new POI();
            poi.id = rs.getInt(1); //TODO: ID SHOULD BE LONG!
            poi.userId = rs.getInt(2); //TODO: ID SHOULD BE LONG!
            poi.lat = rs.getDouble(3);
            poi.lon = rs.getDouble(4);
            poi.timestamp = rs.getLong(5);
            poi.type = POI.Type.values()[rs.getInt(6)];
            poi.dangerLevel = rs.getInt(7);
            
            result.add(poi);
        }
        closeQuery();
        return result;
    }
        
}
