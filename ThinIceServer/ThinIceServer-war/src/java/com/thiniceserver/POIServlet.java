package com.thiniceserver;

import com.thiniceserver.model.POI;
import com.thiniceserver.db.DBAccess;
import java.io.IOException;
import java.io.StringReader;
import java.security.Policy;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class POIServlet extends HttpServlet {

    
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            
            Long since = 0L;
            if (req.getParameterMap().containsKey("since")) {
                since = Long.parseLong(req.getParameter("since"));
            }
            
//        if (req.getParameterMap().containsKey("filter")) {
//            String[] poiTypes = req.getParameter("filter").split("\\|");
//
//            HashSet<Integer> ids = new HashSet<>();
//            
//            for (String t : poiTypes) {
//                POI.Type type = POI.Type.Unknown;
//                try {
//                    type = POI.Type.valueOf(t);
//                } catch (Exception e) {}
//                ids.add(type.ordinal());
//            }
//            
////            for (Integer i : ids) {
////                resp.getWriter().print(i);
////                resp.getWriter().print("<br>");
////            }
//        }
        try {
            DBAccess db = DBAccess.create();
            ArrayList<POI> data = db.getPOIs(since);
            resp.setContentType("application/json");
            resp.getWriter().print(POI.arrayListToJson(data));
        } catch (SQLException ex) {
            Logger.getLogger(POIServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        try {
//            String data = req.getParameter("data");
//            if (data != null) {
//                DBAccess db = DBAccess.create();
//                JsonObject jsonObject = Json.createReader(new StringReader(data)).readObject();
//                POI poi = db.addPOI(POI.fromJson(jsonObject));
//                resp.setContentType("application/json");
//                resp.getWriter().print(POI.toJson(poi));
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(POIServlet.class.getName()).log(Level.SEVERE, null, ex);
//        }

        Long since = 0L;
        if (req.getParameterMap().containsKey("since")) {
            since = Long.parseLong(req.getParameter("since"));
        }
            
        try {
            DBAccess db = DBAccess.create();
            ArrayList<POI> data = db.getPOIs(since);
            resp.setContentType("application/json");
            resp.getWriter().print(POI.arrayListToJson(data));
        } catch (SQLException ex) {
            Logger.getLogger(POIServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
