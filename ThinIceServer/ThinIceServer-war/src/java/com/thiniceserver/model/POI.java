package com.thiniceserver.model;

import com.thiniceserver.db.DBAccess;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

public class POI extends com.thinice.model.POI {
    
    public JsonObject toJson() {
        return Json.createObjectBuilder()
                .add("id", this.id)
                .add("userId", this.userId)
                .add("timestamp", this.timestamp)
                .add("dangerLevel", this.dangerLevel)
                .add("type", this.type.ordinal())
                .add("lat", this.lat)
                .add("lon", this.lon)
                .build();
    }
    
    public static JsonObject toJson(POI poi) {
        return poi.toJson();
    }
    
    public static POI fromJson(JsonObject json) {
        POI poi = new POI();
        poi.id = json.getInt("id", 0);
        poi.userId = json.getInt("userId", 0);
        poi.timestamp = json.getJsonNumber("timestamp").longValue();
        poi.dangerLevel = json.getInt("dangerLevel");
        poi.type = POI.aquirePOITypeById(json.getInt("type"));
        poi.lat = json.getJsonNumber("lat").doubleValue();
        poi.lon = json.getJsonNumber("lon").doubleValue();
        return poi;
    }
    
    public static ArrayList<POI> fromJsonArray(JsonArray jsonArray)
    {
        ArrayList<POI> lst = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); ++i) {
            lst.add(POI.fromJson(jsonArray.getJsonObject(i)));
        }
        return lst;
    }
        
    public static JsonArray arrayListToJson(ArrayList<POI> lst)
    {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (POI poi : lst) {
            arrayBuilder.add(poi.toJson());
        }
        return arrayBuilder.build();
    }
}
