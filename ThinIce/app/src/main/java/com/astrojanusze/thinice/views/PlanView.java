package com.astrojanusze.thinice.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.astrojanusze.thinice.R;
import com.astrojanusze.thinice.control.ContactManager;
import com.astrojanusze.thinice.model.ContactData;

import java.util.ArrayList;

/**
 * Created by Deimos on 2016-04-23.
 */
public class PlanView extends FrameLayout implements View.OnClickListener {

    private ContactManager mManager;
    private Context mContext;
    private NavButton bTeam, bEmergency, bSave;

    public PlanView(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    public PlanView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init();
    }

    public PlanView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_plan, null);
        addView(view);

        mManager = ContactManager.getInstance();

        bTeam = (NavButton) findViewById(R.id.bTeam);
        bEmergency = (NavButton) findViewById(R.id.bContact);
        bSave = (NavButton) findViewById(R.id.bSave);

        bTeam.setOnClickListener(this);
        bEmergency.setOnClickListener(this);
        bSave.setOnClickListener(this);
    }

    private void buildTeamDialog() {
        ArrayList<ContactData> mTeamContacts = mManager.getTeamContacts();
        CharSequence[] items = new CharSequence[mTeamContacts.size()];
        boolean[] checked = new boolean[mTeamContacts.size()];

        for (ContactData con : mTeamContacts) {
            items[mTeamContacts.indexOf(con)] = con.getNumber() + ": " + con.getName();
            checked[mTeamContacts.indexOf(con)] = con.isInTeam();
        }

        TeamDialog dialog = new TeamDialog();
        Bundle bun = new Bundle();
        bun.putCharSequenceArray(TeamDialog.KEY_ARRAY, items);
        bun.putBooleanArray(TeamDialog.KEY_VALS, checked);

        dialog.setArguments(bun);
        dialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "team");
    }

    private void buildEmergencyDialog() {
        ArrayList<ContactData> mEmergencyCandidates = mManager.getEmergencyCandidates();
        CharSequence[] items = new CharSequence[mEmergencyCandidates.size()];
        int selectedIndex = 0;

        for (ContactData con : mEmergencyCandidates) {
            items[mEmergencyCandidates.indexOf(con)] = con.getNumber() + ": " + con.getName();
            if (con.isEmergency())
                selectedIndex = mEmergencyCandidates.indexOf(con);
        }

        EmergencyDialog dialog = new EmergencyDialog();
        Bundle bun = new Bundle();
        bun.putCharSequenceArray(EmergencyDialog.KEY_ARRAY, items);
        bun.putInt(EmergencyDialog.KEY_VAL, selectedIndex);

        dialog.setArguments(bun);
        dialog.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "emergency");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.bTeam:
                buildTeamDialog();
                break;

            case R.id.bContact:
                buildEmergencyDialog();
                break;

            case R.id.bSave:

                break;

        }

    }


    public static class TeamDialog extends DialogFragment {
        public static String KEY_ARRAY = "listItems42";
        public static String KEY_VALS = "boolVals42";
        private CharSequence[] mTeamContacts;
        private boolean[] mCheckedVals;
        private boolean[] mReturnState;

        @Override
        public void setArguments(Bundle bun) {
            mTeamContacts = bun.getCharSequenceArray(KEY_ARRAY);
            mCheckedVals = bun.getBooleanArray(KEY_VALS);
            mReturnState = mCheckedVals.clone();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {


            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Select team members")
                    .setMultiChoiceItems(mTeamContacts, mCheckedVals,
                            new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which,
                                                    boolean isChecked) {

                                    mReturnState[which] = isChecked;

                                }
                            })
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            ContactManager.getInstance().setTeamMembers(mReturnState);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });

            return builder.create();
        }
    }

    public static class EmergencyDialog extends DialogFragment {
        public static String KEY_ARRAY = "listItems24";
        public static String KEY_VAL = "boolVal24";
        private CharSequence[] mTeamContacts;
        private int selectedIndex;

        @Override
        public void setArguments(Bundle bun) {
            mTeamContacts = bun.getCharSequenceArray(KEY_ARRAY);
            selectedIndex = bun.getInt(KEY_VAL);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Select team members")
                    .setSingleChoiceItems(mTeamContacts, selectedIndex, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedIndex = which;
                        }
                    })
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            ContactManager.getInstance().setEmergencyContact(selectedIndex);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });

            return builder.create();
        }
    }

}
