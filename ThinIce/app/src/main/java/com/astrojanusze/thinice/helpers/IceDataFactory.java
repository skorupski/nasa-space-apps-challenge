package com.astrojanusze.thinice.helpers;

import com.astrojanusze.thinice.model.IceData;
import com.astrojanusze.thinice.model.Location;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Michał on 23.04.2016.
 */
public class IceDataFactory {

    public static ArrayList<IceData> createFakeData(InputStream fromGeoJSON) {
        ArrayList<IceData> iceData = new ArrayList<>();
        GeoJSONParser.parse(fromGeoJSON);
        int concentration = 0;
        int thick = 82;
        for (ArrayList<Location> points : GeoJSONParser.polygons) {
            IceData iceRegion = new IceData(concentration, thick, points);
            concentration += 10;
            ++thick;
            iceData.add(iceRegion);
        }
        return iceData;
    }
}
