package com.astrojanusze.thinice.model;

/**
 * Created by Deimos on 2016-04-23.
 */
public class Location {

    private double mLat;
    private double mLon;

    public Location() {}

    public Location(double lat, double lon) {
        this.mLat = lat;
        this.mLon = lon;
    }

    public double getLat() {
        return mLat;
    }

    public void setLat(double mLat) {
        this.mLat = mLat;
    }

    public double getLon() {
        return mLon;
    }

    public void setLon(double mLon) {
        this.mLon = mLon;
    }
}
