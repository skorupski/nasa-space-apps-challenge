package com.astrojanusze.thinice.request;

import android.os.AsyncTask;
import android.util.Log;

import com.astrojanusze.thinice.model.POIData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by j.bielak on 2016-04-24.
 */
public class RequestHelper {

    public interface RequestPOIsListener {
        void onPOIsRecived(ArrayList<POIData> pois);
    }

    public interface RequestAddPOIListener {
        void onPOIAddRecived(POIData poi);
    }

    public static void requestPOIs(long from, RequestPOIsListener listener) {
        new RequestGetPOIsTask(listener).execute(from);
    }

    public static void requestAddPOI(POIData poi, RequestAddPOIListener listener) {
        new RequestAddPOITask(listener).execute(poi);
    }

    static class RequestAddPOITask extends AsyncTask<POIData, Void, String> {

        private final RequestAddPOIListener listener;

        RequestAddPOITask(RequestAddPOIListener listener) {
            this.listener = listener;
        }

        protected String doInBackground(POIData... poiDatas) {
            URL url = null;
            String result = null;
            try {
                url = new URL("http://10.200.35.95:8080/ThinIceServer-war/poi/add");

                URLConnection conn = url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                POIData poi = poiDatas[0];

                Map<String,String> dataToSend = new HashMap<>();
                Log.d("Request", "POI: " + poi.toJson().toString());
                dataToSend.put("data", poi.toJson().toString());

                String encodedData = getEncodedData(dataToSend);
//                Log.d("Dupa", encodedData);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(encodedData);
                writer.flush();
                writer.close();
                os.close();

                BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder total = new StringBuilder(conn.getInputStream().available());
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line);
                }
                result = total.toString();

                conn.connect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                result = e.getMessage();
            } catch (ProtocolException e) {
                e.printStackTrace();
                result = e.getMessage();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                result = e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                result = e.getMessage();
            }
            return result;
        }

        protected void onPostExecute(String result) {
            Log.d("Request", result);
            POIData poi = null;
            try {
                JSONObject obj = new JSONObject(result);
                poi = POIData.fromJson(obj);
            } catch (JSONException e) {
                e.printStackTrace();
                poi = null;
            }
            if (listener != null) {
                listener.onPOIAddRecived(poi);
            }
        }

    }

    private static String getEncodedData(Map<String,String> data) {
        StringBuilder sb = new StringBuilder();
        for(String key : data.keySet()) {
            String value = null;
            try {
                value = URLEncoder.encode(data.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if(sb.length()>0)
                sb.append("&");

            sb.append(key + "=");
            sb.append(value);
        }
        return sb.toString();
    }

    static class RequestGetPOIsTask extends AsyncTask<Long, Void, String> {

        private final RequestPOIsListener listener;

        RequestGetPOIsTask(RequestPOIsListener listener) {
            this.listener = listener;
        }

        protected String doInBackground(Long... fromVal) {
            URL url = null;
            String result = null;
            try {
                url = new URL("http://10.200.35.95:8080/ThinIceServer-war/poi");

                URLConnection conn = url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                Long from = fromVal[0];

                Map<String,String> dataToSend = new HashMap<>();
                dataToSend.put("since", from.toString());

                String encodedData = getEncodedData(dataToSend);
//                Log.d("Dupa", encodedData);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(encodedData);
                writer.flush();
                writer.close();
                os.close();

                BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder total = new StringBuilder(conn.getInputStream().available());
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line);
                }
                result = total.toString();

                conn.connect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                result = e.getMessage();
            } catch (ProtocolException e) {
                e.printStackTrace();
                result = e.getMessage();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                result = e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                result = e.getMessage();
            }
            return result;
        }

        protected void onPostExecute(String result) {
            Log.d("Request", result);

            ArrayList<POIData> pois = new ArrayList<POIData>();
            try {
                JSONArray array = new JSONArray(result);
                for (int i = 0; i < array.length(); i++)
                    pois.add(POIData.fromJson(array.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
                pois = null;
            }
            if (listener != null) {
                listener.onPOIsRecived(pois);
            }
        }

    }
}
