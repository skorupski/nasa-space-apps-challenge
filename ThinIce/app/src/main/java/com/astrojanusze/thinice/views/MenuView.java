package com.astrojanusze.thinice.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.astrojanusze.thinice.R;
import com.astrojanusze.thinice.control.NavigationController;
import com.astrojanusze.thinice.control.StateMachine;
import com.astrojanusze.thinice.model.INavigationElement;
import com.astrojanusze.thinice.model.INavigationScreen;

/**
 * Created by Deimos on 2016-04-23.
 */
public class MenuView extends FrameLayout implements INavigationScreen, View.OnClickListener {

    NavButton bPlan, bDanger, bIce, bPoint;
    NavigationController mNavController;

    public MenuView(Context context) {
        super(context);
        init();
    }

    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_menu, null);
        addView(view);
        loadControls();
        mNavController = new NavigationController() {
            @Override
            public void next() {
                super.next();
                super.select();
            }

            @Override
            public void prev() {
                super.prev();
                super.select();
            }
        };
        mNavController.addNavigationElement(bPlan);
        mNavController.addNavigationElement(bDanger);
        mNavController.addNavigationElement(bIce);
        mNavController.addNavigationElement(bPoint);
    }

    private void loadControls() {
        bPlan = (NavButton) findViewById(R.id.bPlan);
        bDanger = (NavButton) findViewById(R.id.bDanger);
        bIce = (NavButton) findViewById(R.id.bIce);
        bPoint = (NavButton) findViewById(R.id.bPoint);

        bPlan.setOnClickListener(this);
        bDanger.setOnClickListener(this);
        bIce.setOnClickListener(this);
        bPoint.setOnClickListener(this);
    }

    @Override
    public NavigationController getNavigationController() {
        return mNavController;
    }

    @Override
    public void onClick(View v) {

        mNavController.focusElement((INavigationElement)v);

        switch (v.getId()) {

            case R.id.bPlan:
                StateMachine.getInstance().goToState(StateMachine.ViewStates.PlanTrip);
                break;

            case R.id.bDanger:
                StateMachine.getInstance().goToState(StateMachine.ViewStates.DangerMap);
                break;

            case R.id.bIce:
                StateMachine.getInstance().goToState(StateMachine.ViewStates.IceMap);
                break;

            case R.id.bPoint:
                StateMachine.getInstance().goToState(StateMachine.ViewStates.AddPoint);
                break;
        }
    }
}
