package com.astrojanusze.thinice.control;

import com.astrojanusze.thinice.model.Location;

import java.util.ArrayList;

/**
 * Created by Michał on 24.04.2016.
 */
public class LocationTracker {
    private Location mCurrentLocation;
    private ArrayList<Location> mTrace = new ArrayList<>();

    private static LocationTracker instance = null;

    private LocationTracker() {

    }

    public static LocationTracker getInstance() {
        if (instance == null) {
            instance = new LocationTracker();
        }
        return instance;
    }

    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    public ArrayList<Location> getTrace() {
        return mTrace;
    }

    public void setCurrentLocation(Location loc) {
        mCurrentLocation = loc;
        mTrace.add(loc);
    }
}
