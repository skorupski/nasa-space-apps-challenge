package com.astrojanusze.thinice.control;

import com.astrojanusze.thinice.model.INavigationElement;

import java.util.ArrayList;

/**
 * Created by Deimos on 2016-04-23.
 */
public class NavigationController {

    private ArrayList<INavigationElement> mNavElements = new ArrayList<>();
    private INavigationElement mCurrentElement;
    private int index = 0;

    public void addNavigationElement(INavigationElement elem) {
        this.mNavElements.add(elem);
    }

    public void setDefault() {
        index = 0;
        if (mNavElements.size() > 0) {
            mCurrentElement = mNavElements.get(index);
            focusCurrent();
        }
        else
            mCurrentElement = null;
    }

    public void next() {
        if (mNavElements != null && mNavElements.size() > 0) {
            index++;
            if (index > mNavElements.size() - 1)
                index = 0;

            if (mCurrentElement != null)
                mCurrentElement.onUnfocus();
            mCurrentElement = mNavElements.get(index);
            mCurrentElement.onFocus();
        }
    }

    public void prev() {
        if (mNavElements != null && mNavElements.size() > 0) {
            index--;

            if (index < 0)
                index = mNavElements.size() - 1;
            if (mCurrentElement != null)
                mCurrentElement.onUnfocus();
            mCurrentElement = mNavElements.get(index);
            mCurrentElement.onFocus();
        }
    }

    public void select() {
        if (mCurrentElement != null)
            mCurrentElement.onClick();
    }


    public void focusElement(INavigationElement elem) {
        if (mNavElements.contains(elem)) {
            mCurrentElement = elem;
            index = mNavElements.indexOf(elem);
            focusCurrent();
        }
    }

    public void focusCurrent() {
        unfocusAll();
        if (mCurrentElement != null)
            mCurrentElement.onFocus();
    }

    private void unfocusAll() {
        for (INavigationElement el : mNavElements)
            el.onUnfocus();
    }

    public void focusByIndex(int ind) {
        if (ind >= 0 && ind < mNavElements.size()) {
            index = ind;
            mCurrentElement = mNavElements.get(index);
            focusCurrent();
        }
    }

    public INavigationElement getCurrentElement() {
        return mCurrentElement;
    }

}
