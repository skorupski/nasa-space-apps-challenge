package com.astrojanusze.thinice.model;

/**
 * Created by Deimos on 2016-04-23.
 */
public interface INavigationElement {
    void onClick();
    void onFocus();
    void onUnfocus();
}
