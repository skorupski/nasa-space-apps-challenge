package com.astrojanusze.thinice.helpers;

import android.text.TextUtils;

import com.astrojanusze.thinice.model.Location;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by Michał on 23.04.2016.
 */
public class GeoJSONParser {

    public static ArrayList<ArrayList<Location>> polygons = new ArrayList<>();

    public static void parse(InputStream inputStream) {

        polygons.clear();

        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }

            inputStream.close();

            // Parse JSON
            JSONObject json = new JSONObject(sb.toString());
            JSONArray features = json.getJSONArray("features");
            for (int i = 0; i < features.length(); ++i) {
                JSONObject feature = features.getJSONObject(i);
                JSONObject geometry = feature.getJSONObject("geometry");
                if (geometry != null) {
                    String type = geometry.getString("type");

                    // Parse polygons
                    if (!TextUtils.isEmpty(type) && type.equalsIgnoreCase("Polygon")) {

                        // Get the Coordinates
                        JSONArray cords = geometry.getJSONArray("coordinates").getJSONArray(0);
                        ArrayList<Location> points = new ArrayList<>();
                        for (int lc = 0; lc < cords.length(); lc++) {
                            JSONArray cord = cords.getJSONArray(lc);
                            Location latLng = new Location(cord.getDouble(1), cord.getDouble(0));
                            points.add(latLng);
                        }
                        polygons.add(points);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
