package com.astrojanusze.thinice.helpers;

/**
 * Created by Deimos on 2016-04-23.
 */
public class IceHelper {

    public static String getThicknessDescription(final int St) {

        String result = "";

        switch (St) {
            case 82:
                result = "< 30cm";
                break;
            case 83:
                result = "1 - 30cm";
                break;
            case 84:
                result = "10 - 15cm";
                break;
            case 85:
                result = "15 - 30cm";
                break;
            case 86:
                result = "30 - 200cm";
                break;
            case 87:
                result = "30 - 70cm";
                break;
            case 88:
                result = "30 - 50cm";
                break;
            case 89:
                result = "50 - 70cm";
                break;
            case 91:
                result = "70 - 120cm";
                break;
            case 93:
                result = "> 120cm";
                break;
        }
        return result;
    }
}
