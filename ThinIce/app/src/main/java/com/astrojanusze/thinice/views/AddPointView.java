package com.astrojanusze.thinice.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.astrojanusze.thinice.R;
import com.astrojanusze.thinice.control.NavigationController;
import com.astrojanusze.thinice.control.POIManager;
import com.astrojanusze.thinice.control.StateMachine;
import com.astrojanusze.thinice.model.INavigationElement;
import com.astrojanusze.thinice.model.INavigationScreen;
import com.astrojanusze.thinice.model.POIData;

/**
 * Created by Deimos on 2016-04-23.
 */
public class AddPointView extends FrameLayout implements INavigationScreen, View.OnClickListener {

    NavButton bThinIce, bIceHole, bCrevasse, bHuntingGame, bDangerousAnimals, bOtherDanger;
    NavigationController mNavController;

    public AddPointView(Context context) {
        super(context);
        init();
    }

    public AddPointView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AddPointView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_addpoint, null);
        addView(view);
        loadControls();
        mNavController = new NavigationController();
        mNavController.addNavigationElement(bThinIce);
        mNavController.addNavigationElement(bIceHole);
        mNavController.addNavigationElement(bCrevasse);
        mNavController.addNavigationElement(bOtherDanger);
        mNavController.addNavigationElement(bDangerousAnimals);
        mNavController.addNavigationElement(bHuntingGame);

        bThinIce.setOnClickListener(this);
        bIceHole.setOnClickListener(this);
        bCrevasse.setOnClickListener(this);
        bHuntingGame.setOnClickListener(this);
        bDangerousAnimals.setOnClickListener(this);
        bOtherDanger.setOnClickListener(this);
    }

    private void loadControls() {
        bThinIce = (NavButton) findViewById(R.id.bThinIce);
        bIceHole = (NavButton) findViewById(R.id.bIceHole);
        bCrevasse = (NavButton) findViewById(R.id.bCrevasse);
        bHuntingGame = (NavButton) findViewById(R.id.bHuntingGame);
        bDangerousAnimals = (NavButton) findViewById(R.id.bDangerousAnimals);
        bOtherDanger = (NavButton) findViewById(R.id.bOtherDanger);
    }

    @Override
    public NavigationController getNavigationController() {
        return mNavController;
    }

    @Override
    public void onClick(View v) {

        mNavController.focusElement((INavigationElement)v);

        switch (v.getId()) {
            case R.id.bThinIce:
                POIManager.getInstance().addPoi(POIData.PoiType.ThinIce);
                break;
            case R.id.bIceHole:
                POIManager.getInstance().addPoi(POIData.PoiType.IceHole);
                break;
            case R.id.bCrevasse:
                POIManager.getInstance().addPoi(POIData.PoiType.Crevass);
                break;
            case R.id.bHuntingGame:
                POIManager.getInstance().addPoi(POIData.PoiType.HuntingGame);
                break;
            case R.id.bDangerousAnimals:
                POIManager.getInstance().addPoi(POIData.PoiType.DangerousAnimals);
                break;
            case R.id.bOtherDanger:
                POIManager.getInstance().addPoi(POIData.PoiType.Danger);
                break;
        }

        StateMachine.getInstance().goToState(StateMachine.ViewStates.DangerMap);
    }
}
