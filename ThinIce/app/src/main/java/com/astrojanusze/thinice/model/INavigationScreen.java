package com.astrojanusze.thinice.model;

import com.astrojanusze.thinice.control.NavigationController;

import java.util.ArrayList;

/**
 * Created by Deimos on 2016-04-23.
 */
public interface INavigationScreen {
    NavigationController getNavigationController();
}
