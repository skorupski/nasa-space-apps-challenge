package com.astrojanusze.thinice.helpers;

import com.astrojanusze.thinice.model.ContactData;
import com.astrojanusze.thinice.model.Location;
import com.astrojanusze.thinice.model.POIData;

import java.util.ArrayList;

/**
 * Created by Michał on 24.04.2016.
 */
public class ContactDataFactory {

    public static ArrayList<ContactData> createFakeContacts() {
        ArrayList<ContactData> contacts = new ArrayList<>();

        contacts.add(new ContactData("John Malkovich", "602-124-111"));
        contacts.add(new ContactData("John Travolta", "342-001-456"));
        contacts.add(new ContactData("John Lennon", "921-021-345"));

        return contacts;
    }
}
