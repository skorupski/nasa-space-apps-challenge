package com.astrojanusze.thinice.helpers;

import com.astrojanusze.thinice.model.Location;
import com.astrojanusze.thinice.model.POIData;

import java.util.ArrayList;

/**
 * Created by Michał on 24.04.2016.
 */
public class POIDataFactory {

    static Location locs[] = {new Location(52.244988, 21.045814), new Location(52.243595, 21.035643),
                           new Location(52.245176, 21.055814), new Location(52.233595, 21.045143),
                           new Location(52.254988, 21.031714), new Location(52.231225, 21.025643)
        };

    public static ArrayList<POIData> createFakePOI() {
        ArrayList<POIData> points = new ArrayList<>();
        int i = 0;
        for(POIData.PoiType value : POIData.PoiType.values()) {
            POIData point = new POIData(locs[i].getLat(), locs[i].getLon(), value, POIData.PoiDanger.Risk, System.currentTimeMillis() );
            ++i;
            points.add(point);
        }
        return points;
    }
}
