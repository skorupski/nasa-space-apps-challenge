package com.astrojanusze.thinice.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.astrojanusze.thinice.R;
import com.astrojanusze.thinice.control.NavigationController;
import com.astrojanusze.thinice.control.StateMachine;
import com.astrojanusze.thinice.model.INavigationElement;
import com.astrojanusze.thinice.model.INavigationScreen;

/**
 * Created by Deimos on 2016-04-23.
 */
public class IceLegendView extends FrameLayout {

    public IceLegendView(Context context) {
        super(context);
        init();
    }

    public IceLegendView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public IceLegendView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_ice, null);
        addView(view);
    }

}
