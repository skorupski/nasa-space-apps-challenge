package com.astrojanusze.thinice.model;

import com.astrojanusze.thinice.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Deimos on 2016-04-23.
 */
public class POIData {

    public static final String POI_LAT = "lat";
    public static final String POI_LON = "lon";
    public static final String POI_TIME = "timestamp";
    public static final String POI_TYPE = "type";
    public static final String POI_DANGER = "dangerLevel";

    public enum PoiType {
        ThinIce, IceHole, Crevass, HuntingGame, DangerousAnimals, Danger;
        private int[] icons = new int[]{
                R.drawable.poi_thinice,
                R.drawable.poi_icehole,
                R.drawable.poi_crevasse,
                R.drawable.poi_hunting,
                R.drawable.poi_animals,
                R.drawable.poi_danger
        };

        public int getIcon() {
            return icons[this.ordinal()];
        }
    }

    public enum PoiDanger {Risk, NoRisk}

    private Location mLocation;
    private PoiType mType;
    private PoiDanger mRisk;
    private long mTimeStamp;

    public POIData() {
    }

    public POIData(double lat, double lon, PoiType type, PoiDanger risk, long timesttamp) {
        this.mLocation = new Location(lat, lon);
        this.mType = type;
        this.mRisk = risk;
        this.mTimeStamp = timesttamp;
    }

    public PoiDanger getRisk() {
        return mRisk;
    }

    public void setRisk(PoiDanger mRisk) {
        this.mRisk = mRisk;
    }

    public PoiType getType() {
        return mType;
    }

    public void setType(PoiType mType) {
        this.mType = mType;
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }

    public void setTimeStamp(long mTimeStamp) {
        this.mTimeStamp = mTimeStamp;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location mLocation) {
        this.mLocation = mLocation;
    }

    public static POIData fromJson(JSONObject obj) {
        POIData poi = null;
        try {
            poi = new POIData();
            poi.setTimeStamp(obj.getLong(POI_TIME));
            poi.setType(POIData.PoiType.values()[obj.getInt(POI_TYPE)]);
            poi.setRisk(POIData.PoiDanger.values()[obj.getInt(POI_DANGER)]);
            poi.setLocation(new Location(obj.getDouble(POI_LAT), obj.getDouble(POI_LON)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return poi;
    }

    public JSONObject toJson() {
        JSONObject json = null;
        try {
            json = new JSONObject();
            json.put(POI_TIME, this.getTimeStamp());
            json.put(POI_DANGER, this.getRisk().ordinal());
            json.put(POI_TYPE, this.getType().ordinal());
            json.put(POI_LAT, this.getLocation().getLat());
            json.put(POI_LON, this.getLocation().getLon());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

}
