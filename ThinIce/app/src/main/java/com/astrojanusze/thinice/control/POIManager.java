package com.astrojanusze.thinice.control;

import com.astrojanusze.thinice.helpers.ApiParser;
import com.astrojanusze.thinice.model.POIData;
import com.astrojanusze.thinice.request.RequestHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Deimos on 2016-04-23.
 */
public class  POIManager {

    public interface OnPOIUpdateListener {
        void poiAdded(POIData POI);
        void requestedPoiReady(ArrayList<POIData> pois);
    }

    private OnPOIUpdateListener mUpdateListener;
    private static POIManager instance;

    ArrayList<POIData> mPoints = new ArrayList<>();

    public static POIManager getInstance() {
        if (instance == null)
            instance = new POIManager();

        return instance;
    }

    public void addPoi(POIData.PoiType type) {
        POIData poi = new POIData();
        poi.setType(type);
        poi.setRisk(type == POIData.PoiType.HuntingGame ? POIData.PoiDanger.NoRisk : POIData.PoiDanger.Risk);
        poi.setTimeStamp(new Date().getTime());
        poi.setLocation(LocationTracker.getInstance().getCurrentLocation());

        RequestHelper.requestAddPOI(poi, new RequestHelper.RequestAddPOIListener() {
            @Override
            public void onPOIAddRecived(POIData poi) {
                if (mUpdateListener != null) {
                    mUpdateListener.poiAdded(poi);
                }
            }
        });
    }

    public void requestPOIupdate() {
        RequestHelper.requestPOIs(0, new RequestHelper.RequestPOIsListener() {
            @Override
            public void onPOIsRecived(ArrayList<POIData> pois) {
                if (mUpdateListener != null) {
                    mUpdateListener.requestedPoiReady(pois);
                }
            }
        });
    }

    public void setOnPoiUpdateListener(OnPOIUpdateListener listener) {
        mUpdateListener = listener;
    }
}
