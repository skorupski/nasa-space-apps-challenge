package com.astrojanusze.thinice.model;

/**
 * Created by Deimos on 2016-04-24.
 */
public class ContactData {

    private String mName;
    private String mNumber;
    private boolean isInTeam;
    private boolean isEmergency;

    public ContactData() {}
    public ContactData(String name, String nr) {
        this.mName = name;
        this.mNumber = nr;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String mNumber) {
        this.mNumber = mNumber;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public boolean isInTeam() {
        return isInTeam;
    }

    public void setInTeam(boolean inTeam) {
        isInTeam = inTeam;
    }

    public boolean isEmergency() {
        return isEmergency;
    }

    public void setEmergency(boolean emergency) {
        isEmergency = emergency;
    }
}
