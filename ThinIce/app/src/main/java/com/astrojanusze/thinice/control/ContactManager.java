package com.astrojanusze.thinice.control;

import com.astrojanusze.thinice.helpers.ApiParser;
import com.astrojanusze.thinice.helpers.ContactDataFactory;
import com.astrojanusze.thinice.model.ContactData;
import com.astrojanusze.thinice.model.POIData;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Deimos on 2016-04-23.
 */
public class ContactManager {

    private static ContactManager instance;
    private ArrayList<ContactData> mContacts;

    public static ContactManager getInstance() {
        if (instance == null)
            instance = new ContactManager();

        return instance;
    }

    private ContactManager() {
        this.mContacts = ContactDataFactory.createFakeContacts();
    }

    public ArrayList<ContactData> getTeamContacts() {
        ArrayList<ContactData> list = new ArrayList<ContactData>();

        for (ContactData contact : mContacts)
        if (!contact.isEmergency())
            list.add(contact);

        return list;
    }

    public ContactData getEmergencyContact() {

        for (ContactData contact : mContacts)
            if (contact.isEmergency())
                return contact;

        return null;
    }

    public ArrayList<ContactData> getEmergencyCandidates() {
        ArrayList<ContactData> list = new ArrayList<ContactData>();

        for (ContactData contact : mContacts)
            if (!contact.isInTeam())
                list.add(contact);

        return list;
    }

    public void setTeamMembers(boolean[] indexes) {
        ArrayList<ContactData> teams = getTeamContacts();
        for (int i = 0; i < indexes.length; i++) {
            teams.get(i).setInTeam(indexes[i]);
        }
    }

    public void setEmergencyContact(int index) {
        ArrayList<ContactData> teams = getEmergencyCandidates();

        for (int i = 0; i < teams.size(); i++) {
            if (i == index)
                teams.get(i).setEmergency(true);
            else
                teams.get(i).setEmergency(false);
        }
    }



}
