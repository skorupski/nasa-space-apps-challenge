package com.astrojanusze.thinice.control;

import android.view.View;

import java.util.HashMap;

/**
 * Created by Deimos on 2016-04-23.
 */
public class StateMachine {

    private static StateMachine instance;

    public static StateMachine init() {
        if (instance == null)
            instance = new StateMachine();

        return instance;
    }

    public static StateMachine getInstance() {
        return instance;
    }

    public interface IStateManager {
        void onStateChanged(ViewStates states, Object data);
    }

    public IStateManager mListener;
    public enum ViewStates { PlanTrip, DangerMap, IceMap, AddPoint }
    private HashMap<ViewStates, View> mViewPairs = new HashMap<>();

    private ViewStates mCurrentState;

    public void goToState(ViewStates state) {

        if (mCurrentState == state)
            return;

        if (mCurrentState != null)
            mViewPairs.get(mCurrentState).setVisibility(View.GONE);

        mCurrentState = state;
        View view = mViewPairs.get(state);
        view.setVisibility(View.VISIBLE)
        ;
        if (mListener != null)
            mListener.onStateChanged(state, view);
    }

    public void registerState(ViewStates state, View navScreen) {
        mViewPairs.put(state, navScreen);
    }

    public ViewStates getmCurrentState() {
        return mCurrentState;
    }

    public void registerStateChangedListener(IStateManager listener) {
        this.mListener = listener;
    }
}
