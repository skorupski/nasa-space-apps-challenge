package com.astrojanusze.thinice.model;

import com.astrojanusze.thinice.R;
import com.astrojanusze.thinice.helpers.IceHelper;

import java.util.ArrayList;

/**
 * Created by Deimos on 2016-04-23.
 */
public class IceData {

    private static int[] mColors = new int[]{
            R.color.ice_0,
            R.color.ice_1,
            R.color.ice_2,
            R.color.ice_3,
            R.color.ice_4,
            R.color.ice_5,
            R.color.ice_6,
            R.color.ice_7,
            R.color.ice_8,
            R.color.ice_9
    };
    private ArrayList<Location> mGeometry;
    private int mConcentration;
    private int mThickness;

    public IceData() {
    }

    public IceData(int conc, int thick, ArrayList<Location> points) {
        this.mConcentration = conc;
        this.mThickness = thick;
        this.mGeometry = points;
    }

    public int getConcentrationColor() {
        return mColors[mConcentration / 10];
    }

    public String getThicknessDescription() {
        return IceHelper.getThicknessDescription(this.mThickness);
    }

    public ArrayList<Location> getGeometry() {
        return mGeometry;
    }

    public void setGeometry(ArrayList<Location> mGeometry) {
        this.mGeometry = mGeometry;
    }
}
