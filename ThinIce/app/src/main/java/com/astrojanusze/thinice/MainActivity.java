package com.astrojanusze.thinice;

import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.astrojanusze.thinice.control.LocationTracker;
import com.astrojanusze.thinice.control.NavigationController;
import com.astrojanusze.thinice.control.POIManager;
import com.astrojanusze.thinice.control.StateMachine;
import com.astrojanusze.thinice.helpers.IceDataFactory;
import com.astrojanusze.thinice.helpers.POIDataFactory;
import com.astrojanusze.thinice.model.INavigationScreen;
import com.astrojanusze.thinice.model.IceData;
import com.astrojanusze.thinice.model.POIData;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.MyBearingTracking;
import com.mapbox.mapboxsdk.constants.MyLocationTracking;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.TrackingSettings;
import com.mapbox.mapboxsdk.maps.UiSettings;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MapboxMap.OnMyLocationChangeListener, MapboxMap.OnMyLocationTrackingModeChangeListener {

    private MapView mapView;
    private View mMenuView, mAddPointView, mPlanView, mIceView, mDangerView;
    private View mTrackMeButton;

    private StateMachine mStateMachine;
    private StateMachine.IStateManager mStateManager;

    private NavigationController mCurrentNavigation;

    private MapboxMap mMapboxMap;
    private Location mLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        mStateMachine = StateMachine.init();
        loadControls();

        mTrackMeButton = findViewById(R.id.trackMeArrow);
        mTrackMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CameraPosition normalCameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()))
                        .zoom(15)
                        .build();
                mMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(normalCameraPosition), 1000, new MapboxMap.CancelableCallback() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onFinish() {
                        final Handler h = new Handler();
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                TrackingSettings trackingSettings = mMapboxMap.getTrackingSettings();
                                trackingSettings.setMyLocationTrackingMode(MyLocationTracking.TRACKING_FOLLOW);
                                trackingSettings.setMyBearingTrackingMode(MyBearingTracking.COMPASS);
                            }
                        }, 10);

                    }
                });
            }
        });

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mMapboxMap = mapboxMap;
                // Customize map with markers, polylines, etc.

                UiSettings uiSettings = mMapboxMap.getUiSettings();
                uiSettings.setTiltGesturesEnabled(false);
                uiSettings.setZoomControlsEnabled(false);
                uiSettings.setLogoEnabled(false);

                TrackingSettings trackingSettings = mMapboxMap.getTrackingSettings();
                trackingSettings.setMyLocationTrackingMode(MyLocationTracking.TRACKING_FOLLOW);
                trackingSettings.setMyBearingTrackingMode(MyBearingTracking.COMPASS);
                trackingSettings.setDismissTrackingOnGesture(true);

                mMapboxMap.setOnMyLocationChangeListener(MainActivity.this);
                try {
                    mapboxMap.setMyLocationEnabled(true);
                } catch (SecurityException e) {
                    Toast.makeText(MainActivity.this,
                            "Location permission is not available", Toast.LENGTH_SHORT).show();
                    finish();
                }

                mMapboxMap.setOnMyLocationTrackingModeChangeListener(MainActivity.this);

                POIManager.getInstance().setOnPoiUpdateListener(new POIManager.OnPOIUpdateListener() {
                    @Override
                    public void poiAdded(POIData point) {
                        IconFactory iconFactory = IconFactory.getInstance(MainActivity.this);
                        Drawable iconDrawable = ContextCompat.getDrawable(MainActivity.this, point.getType().getIcon());
                        Icon icon = iconFactory.fromDrawable(iconDrawable);
                        Marker marker = mMapboxMap.addMarker(new MarkerOptions().position(new LatLng(point.getLocation().getLat(), point.getLocation().getLon())).icon(icon));
                        markers.add(marker);
                    }

                    @Override
                    public void requestedPoiReady(ArrayList<POIData> pois) {
                        markers.clear();
                        for (POIData point : pois) {
                            IconFactory iconFactory = IconFactory.getInstance(MainActivity.this);
                            Drawable iconDrawable = ContextCompat.getDrawable(MainActivity.this, point.getType().getIcon());
                            Icon icon = iconFactory.fromDrawable(iconDrawable);
                            Marker marker = mMapboxMap.addMarker(new MarkerOptions().position(new LatLng(point.getLocation().getLat(), point.getLocation().getLon())).icon(icon));
                            markers.add(marker);
                        }
                    }


                });

                showPOIonMap();
            }
        });
    }

    private void loadControls() {

        createViews();

        mStateManager = new StateMachine.IStateManager() {
            @Override
            public void onStateChanged(StateMachine.ViewStates states, Object data) {

                if (states != StateMachine.ViewStates.AddPoint)
                    mCurrentNavigation = ((INavigationScreen)mMenuView).getNavigationController();

                if (mMapboxMap != null) {
                    mMapboxMap.removeAnnotations();
                    hidePOIonMap();
                }
                switch (states) {

                    case AddPoint:
                        mAddPointView.setVisibility(View.VISIBLE);
                        mCurrentNavigation = ((INavigationScreen) mAddPointView).getNavigationController();
                        mCurrentNavigation.setDefault();
                        break;

                    case DangerMap:
                        // change map layer
                        mCurrentNavigation.focusByIndex(1);
                        showPOIonMap();
                        break;

                    case IceMap:
                        // change map layer
                        mCurrentNavigation.focusByIndex(2);
                        new DrawPolygons().execute();
                        break;

                    case PlanTrip:
                        // change map layer
                        break;

                }

            }
        };

        mStateMachine.registerStateChangedListener(mStateManager);

        mMenuView = findViewById(R.id.layMenu);
        mAddPointView = findViewById(R.id.layPoint);
        mPlanView = findViewById(R.id.layPlan);
        mDangerView = findViewById(R.id.layDanger);
        mIceView = findViewById(R.id.layIce);

        mStateMachine.registerState(StateMachine.ViewStates.PlanTrip, mPlanView);
        mStateMachine.registerState(StateMachine.ViewStates.AddPoint, mAddPointView);
        mStateMachine.registerState(StateMachine.ViewStates.DangerMap, mDangerView);
        mStateMachine.registerState(StateMachine.ViewStates.IceMap, mIceView);

        mCurrentNavigation = ((INavigationScreen) mMenuView).getNavigationController();

        mCurrentNavigation.setDefault();
        mCurrentNavigation.next();
    }

    private void createViews() {

    }

    // Add the mapView lifecycle to the activity's lifecycle methods
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (mStateMachine.getmCurrentState() == StateMachine.ViewStates.AddPoint)
            mStateMachine.goToState(StateMachine.ViewStates.IceMap);
        else
            super.onBackPressed();
    }

    public void onMyLocationChange(@Nullable Location location) {
        if (location != null) {
            if (mLocation == null) {
                // initial location to reposition map

                CameraPosition normalCameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(location.getLatitude(), location.getLongitude()))
                        .zoom(15)
                        .build();
                mMapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(normalCameraPosition));
            }
            mLocation = location;
            LocationTracker tracker = LocationTracker.getInstance();
            if (tracker != null) {
                LocationTracker.getInstance().setCurrentLocation(new com.astrojanusze.thinice.model.Location(location.getLatitude(), location.getLongitude()));
            }
        }
    }

    @Override
    public void onMyLocationTrackingModeChange(int myLocationTrackingMode) {
        if (myLocationTrackingMode == MyLocationTracking.TRACKING_NONE) {
            mTrackMeButton.setVisibility(View.VISIBLE);
        } else {
            mTrackMeButton.setVisibility(View.GONE);
        }
    }

    List<Marker> markers = new ArrayList<>();

    private void showPOIonMap() {
        if (mMapboxMap != null) {
            POIManager.getInstance().requestPOIupdate();
        }
    }

    private void hidePOIonMap() {
        if (mMapboxMap != null) {
            for(Marker marker : markers) {
                mMapboxMap.removeMarker(marker);
            }
        }
    }

    private class DrawPolygons extends AsyncTask<Void, Void, List<PolygonOptions>> {
        @Override
        protected List<PolygonOptions> doInBackground(Void... voids) {
            List<PolygonOptions> polygons = null;
            // Load GeoJSON file
            try {
                InputStream inputStream = MainActivity.this.getAssets().open("polygons.json");
                List<IceData> iceRegions = IceDataFactory.createFakeData(inputStream);
                if (iceRegions != null) {
                    polygons = new ArrayList<>();
                    for (IceData iceRegion : iceRegions) {
                        PolygonOptions polygon = new PolygonOptions();
                        for (com.astrojanusze.thinice.model.Location loc : iceRegion.getGeometry()) {
                            polygon.add(new LatLng(loc.getLat(), loc.getLon()));
                        }
                        polygon.fillColor(MainActivity.this.getResources().getColor(iceRegion.getConcentrationColor()));
                        polygons.add(polygon);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return polygons;
        }

        @Override
        protected void onPostExecute(List<PolygonOptions> polygons) {
            super.onPostExecute(polygons);

            // Draw Polygons on MapView
            if (polygons != null) {
                mMapboxMap.addPolygons(polygons);
            }
        }

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN)
                    mCurrentNavigation.prev();
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN)
                    mCurrentNavigation.next();
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }
}
