package com.astrojanusze.thinice.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.Button;

import com.astrojanusze.thinice.R;
import com.astrojanusze.thinice.model.INavigationElement;

/**
 * Created by Deimos on 2016-04-23.
 */
public class NavButton extends Button implements INavigationElement {

    public NavButton(Context context) {
        super(context);
        init();
    }

    public NavButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NavButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setBackgroundResource(R.drawable.bg_navbutton);
    }

    @Override
    public void onClick() {
        if (this.hasOnClickListeners())
            this.callOnClick();
    }

    @Override
    public void onFocus() {
        setBackgroundResource(R.drawable.bg_navbutton_focused);
    }

    @Override
    public void onUnfocus() {
        setBackgroundResource(R.drawable.bg_navbutton);
    }
}
