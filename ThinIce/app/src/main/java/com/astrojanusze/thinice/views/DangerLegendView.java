package com.astrojanusze.thinice.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.astrojanusze.thinice.R;

/**
 * Created by Deimos on 2016-04-23.
 */
public class DangerLegendView extends FrameLayout {

    public DangerLegendView(Context context) {
        super(context);
        init();
    }

    public DangerLegendView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DangerLegendView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_danger, null);
        addView(view);
    }

}
